# -*- coding: utf-8 -*-

import requests


class BaseClient(object):
    def __init__(self, token, base_url):
        self.token = token
        self.base_url = base_url

    def get(self, url, params=None):
        request_params = \
            {
                'oauth_token': self.token
            }
        if params:
            request_params.update(params)
        r = requests.get(self.base_url + url, params=request_params)
        return r.json()


class MoiKrugClient(BaseClient):
    def my_profile(self):
        return self.get('my')

    def my_friends(self):
        return self.get('my/friends/')

    def my_fof(self):
        return self.get('my/fof/')

    def my_positions(self):
        return self.get('my/positions/')

    def get_other_profiles(self, ids):
        if isinstance(ids, list):
            ids = ','.join(ids)
        return self.get('person', params={'ids': ids})

    def get_other_profiles_jobs(self, ids):
        if isinstance(ids, list):
            ids = ','.join(ids)
        return self.get('person/positions/', params={'ids': ids})

    def get_company_info(self, company_id):
        return self.get('company', params={'ids': company_id})

    def get_company_staff(self, company_id):
        return self.get('company/staff', params={'ids': company_id})[0]



