# -*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from ..items import MKrugItem


class MKrugSpider(CrawlSpider):
    name = 'mkrug'
    start_urls = ['http://moikrug.ru/companies/548669435/']

    rules = [
        Rule(SgmlLinkExtractor(allow=r'http://moikrug.ru/companies/(.*)$'), callback='parse_company',
             follow=True),

        Rule(SgmlLinkExtractor(allow=r'http://(.*).moikrug.ru/$'), callback='parse_person',
             follow=True),
        Rule(SgmlLinkExtractor(allow=r'http://moikrug.ru/companies/(.*)$'), follow=True),
    ]

    def parse_company(self, response):
        hxs = HtmlXPathSelector(response)
        title = hxs.select('//*[@id="main"]/div[2]/div[1]/div[1]/h1/text()').extract()
        item = MKrugItem()
        if 1 == len(title):
            print u'%s %s' % (title[0], response.url)
        return item

    def parse_person(self, response):
        hxs = HtmlXPathSelector(response)
        name = hxs.select('//*[@id="main"]/div[1]/div[2]/h1/meta[1]/@content').extract()

        pr_resume = hxs.select('//*[@id="main"]/div[1]/div[3]/p/text()').extract()

        if 1 == len(name):
            print u'%s - %s' % (name[0], pr_resume)
        item = MKrugItem()
        return item
