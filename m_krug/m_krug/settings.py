# Scrapy settings for m_krug project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'm_krug'

LOG_ENABLED = False

SPIDER_MODULES = ['m_krug.spiders']
NEWSPIDER_MODULE = 'm_krug.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'm_krug (+http://www.yourdomain.com)'
